﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        Entities db = new Entities();
        //
        // GET: /Home/
        public ActionResult Index()
        {
            var news = db.News.Where(d => d.IsHidden == false).ToList();
            return View(news);
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            News news = db.News.Find(id);
            if (news == null) 
            {
                return HttpNotFound();
            }
            return View(news);
        }
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string email,string password)
        {
            Entities db = new Entities();
            var user = db.User.Where(d => d.Email == email && d.Password == password).FirstOrDefault();
            if (user != null)
            {
                return RedirectToAction("Index");
            }
            else {
                ViewBag.Message = "帳密有錯,請重新輸入";
                return View();;
            }
            //return View();
        }
	}
}